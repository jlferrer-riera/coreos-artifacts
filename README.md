# coreOS artifacts 
Repo to store compiled modules not included by default on coreOS stable dists. 
It basically includes support for VRF (vrf.ko)

Deployments using ignition can use this as URL to download and load those modules,
without having to compile it.  

Currently supported kernels:

- [kernel-4.18.43-coreos](modules/kernel-4.18.43-coreos)
- [kernel-4.18.50-coreos](modules/kernel-4.18.50-coreos)
